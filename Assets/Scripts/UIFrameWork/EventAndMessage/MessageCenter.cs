//脚本名称: MessageCenter
//功能描述：消息传递中心
//                 负责UI框架中，所有UI窗体之间的数据传值
//作者：
//日期：2021/07/07 15:17:22

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageCenter 
{
    //委托，消息传递
    public delegate void DelMessageDelivery(KeyValueUpdate kv);
    //消息中心缓存集合
    //<string:数据的分类>
    //<DelMessageDelivery 数据执行委托>
    public static Dictionary<string, DelMessageDelivery> _dicMessages = new Dictionary<string, DelMessageDelivery>();

    /// <summary>
    /// 添加消息的监听
    /// </summary>
    /// <param name="messageType">消息分类</param>
    /// <param name="handler">消息委托</param>
    public static void AddMsgListener(string messageType,DelMessageDelivery handler)
    {
        if (!_dicMessages.ContainsKey(messageType))
            _dicMessages.Add(messageType,null);
        _dicMessages[messageType] += handler;
    }

    /// <summary>
    /// 取消消息的监听
    /// </summary>
    /// <param name="messageType"></param>
    /// <param name="handler"></param>
    public static void RemoveMsgListener(string messageType,DelMessageDelivery handler)
    {
        if (_dicMessages.ContainsKey(messageType))
            _dicMessages[messageType] -= handler;
    }

    /// <summary>
    /// 取消所有指定消息的监听
    /// </summary>
    public static void ClearAllMsgListener()
    {
        _dicMessages.Clear();
    }

    /// <summary>
    /// 发送消息
    /// </summary>
    /// <param name="messageType">消息的分类</param>
    /// <param name="kv">键值对对象</param>
    public static void SendMessage(string messageType,KeyValueUpdate kv)
    {
        DelMessageDelivery del;
        if (_dicMessages.TryGetValue(messageType, out del))
        {
            if (del != null)
                del(kv);
        }
    }

}


/// <summary>
/// 键值更新对，配合委托
/// </summary>
public class KeyValueUpdate
{
    //键
    private string _Key;
    //值
    private object _Values;

    public string Key { get => _Key;  }
    public object Values { get => _Values;  }

    public KeyValueUpdate(string key,object valueObj)
    {
        _Key = key;
        _Values = valueObj;
    }

}
    
