//脚本名称: UIType
//功能描述：UI类型
//作者：尚苗苗
//日期：2021/07/05 15:30:26


[System.Serializable]
public class UIType
{
    //是否清空栈集合
    public bool IsClearStack = false;

    public UIFormType uiFormType = UIFormType.Normal;
    public UIFormShowMode uiFormShowMode = UIFormShowMode.Normal;
    public UIFormLucenyType uiFormLucenyType = UIFormLucenyType.Luceny;
}
