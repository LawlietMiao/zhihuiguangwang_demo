//脚本名称: UIManager
//功能描述：
//作者：
//日期：2021/07/05 15:01:15

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoSingleton<UIManager>
{
    //UI窗体的路径
    private Dictionary<string, string> _DicFormsPaths = new Dictionary<string, string>();
    //UI窗体的缓存
    private Dictionary<string, BaseUIForm> _DicAllUIForms = new Dictionary<string, BaseUIForm>();
    //当前显示的UI窗体
    private Dictionary<string, BaseUIForm> _DicCurrentShowUIForms = new Dictionary<string, BaseUIForm>();

    //定义“栈”集合,显示当前所有【反向切换】的窗口类型（具备“反向切换”属性窗体的管理）
    private Stack<BaseUIForm> _stackCurrentUIForms=new Stack<BaseUIForm>();



    //UI根节点
    private Transform _traCanvasTransform = null;
    //全屏显示的节点
    private Transform _traNormal = null;
    //固定显示的节点
    private Transform _traFixed = null;
    //弹出节点
    private Transform _traPopUp = null;
    //UI管理脚本的节点
    private Transform _traUIScripts = null;


    /// <summary>
    /// 初始化核心数据，加载“UI窗口路径”到集合中
    /// </summary>
    private void Awake()
    {
        InitTootCanvasLoading();
        //初始化各个节点的信息
        _traCanvasTransform = GameObject.FindGameObjectWithTag(SysDefine.SYS_TAG_CANVAS).transform;
        _traNormal= UnityHelper.FindTheChildNode(_traCanvasTransform, SysDefine.SYS_NORMAL_NODE);
        _traFixed = UnityHelper.FindTheChildNode(_traCanvasTransform, SysDefine.SYS_FIXED_NODE);
        _traPopUp = UnityHelper.FindTheChildNode(_traCanvasTransform, SysDefine.SYS_POPUP_NODE);
        _traUIScripts = UnityHelper.FindTheChildNode(_traCanvasTransform, SysDefine.SYS_SCRIPTMANAGER_NODE);
        this.transform.SetParent(_traUIScripts, false);
        DontDestroyOnLoad(_traCanvasTransform);
        if (_DicFormsPaths != null)
        {
            InitUIFormPathData();
        }
    }



    #region 公共方法
    /// <summary>
    /// 打开UI窗体
    /// 1、根据UI窗体的名称，加载到所有UI窗口的的缓存集合中
    /// 2、根据不同的UI窗体显示模式，分别做不同的加载处理
    /// </summary>
    public void ShowUIForms(string uiFormName)
    {
        BaseUIForm baseUIForm;         //UI窗体的基类
        //参数的检查
        if (string.IsNullOrEmpty(uiFormName)) return;

        //根据UI窗体的名称，加载到所有UI窗口的的缓存集合中
        baseUIForm = LoadFormsToAllUIFormCatch(uiFormName);
        if (baseUIForm == null) return;

        //是否清空栈集合中的数据
        if (baseUIForm.CurrentUIType.IsClearStack)
            ClearStackArray();

            //根据不同的UI窗体的显示模式，分别作不同的加载处理
            switch (baseUIForm.CurrentUIType.uiFormShowMode)
        {
            case UIFormShowMode.Normal:       //"普通窗口"，当前窗体加载到当前窗体集合中去
                LoadUIToCurrentChche(uiFormName);
                break;
            case UIFormShowMode.ReverseChange:            //反向切换
                PushUIFormToStack(uiFormName);
                break;
            case UIFormShowMode.HideOther:                //隐藏其他
                EnterUIFormsAndHideOther(uiFormName);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 关闭UI窗体(返回上一个窗体)
    /// </summary>
    /// <param name="uiFormName"></param>
    public void CloseUIForms(string uiFormName)
    {
        BaseUIForm baseUIForm;

        //参数检查
        if (string.IsNullOrEmpty(uiFormName)) return;
        //所有UI窗体集合中，入股偶没有记录，则直接返回
        _DicAllUIForms.TryGetValue(uiFormName, out baseUIForm);
        if (baseUIForm == null) return;
        //根据窗体不同的显示类型，分别作不同的关闭处理
        switch (baseUIForm.CurrentUIType.uiFormShowMode)
        {
            case UIFormShowMode.Normal:
                //普通关闭
                ExitUIForms(uiFormName);
                break;
            case UIFormShowMode.ReverseChange:
                PopUIForms();
                //反向切换窗体的关闭
                break;
            case UIFormShowMode.HideOther:
                //隐藏其他窗体的关闭
                ExitUIFormsAndHideOther(uiFormName);
                break;
            default:
                break;
        }
    }
    #endregion

    #region 显示“UI管理器”内部核心数据，测试使用
    /// <summary>
    /// 显示所有UI窗体的数量
    /// </summary>
    /// <returns></returns>
    public int ShowAllUIFormCount()
    {
        if (_DicAllUIForms != null)
            return _DicAllUIForms.Count;
        return 0;
    }
    /// <summary>
    /// 显示当前窗体集合数量
    /// </summary>
    /// <returns></returns>
    public int ShowCurrentUIFormCount()
    {
        if (_DicCurrentShowUIForms != null)
            return _DicCurrentShowUIForms.Count;
        return 0;
    }
    /// <summary>
    /// 显示当前栈集合窗体数量
    /// </summary>
    /// <returns></returns>
    public int ShowCurrentStackUIFormCount()
    {
        if (_stackCurrentUIForms != null)
            return _stackCurrentUIForms.Count;
        return 0;
    }

    #endregion


    #region 私有方法
    /// <summary>
    /// 初始化加载（UI根窗口）Canvas预设
    /// </summary>
    private void InitTootCanvasLoading()
    {
        ResourceMgr.GetInstance.LoadAsset("Canvas");
    }

    /// <summary>
    /// 根据UI窗体的名称，加载到所有UI窗口的的缓存集合中
    /// </summary>
    /// <param name="uiFormName"></param>
    /// <returns></returns>
    private BaseUIForm LoadFormsToAllUIFormCatch(string uiFormName)
    {
        BaseUIForm baseUIResult = null;          //加载的返回UI窗体基类
        _DicAllUIForms.TryGetValue(uiFormName, out baseUIResult);
        if (baseUIResult == null)
        {
            baseUIResult = LoadUIForm(uiFormName);
        }
        return baseUIResult;
    }

    /// <summary>
    /// 加载UI窗体
    /// 根据不同预设克隆体中带的脚本中不同的“位置信息”，加载到“根窗体”下不同的节点
    /// 隐藏刚创建的UI克隆体
    /// 把克隆体加入到“所有UI窗体”缓存结合中
    /// </summary>
    /// <param name="uiFormName">UI窗体的名称</param>
    private BaseUIForm LoadUIForm(string uiFormName)
    {
        string strUIFormPath = null;     //ui窗体查询路径
        GameObject goCloneUIPrefabs = null;          //创建的UI克隆体预设
        BaseUIForm baseUIForm = null;                       //窗体基类

        //根据UI窗体的名称，得到对应的加载路径
        _DicFormsPaths.TryGetValue(uiFormName, out strUIFormPath);
        if (!string.IsNullOrEmpty(strUIFormPath))
            goCloneUIPrefabs = ResourceMgr.GetInstance.LoadAsset(strUIFormPath);
        goCloneUIPrefabs.name= goCloneUIPrefabs.name.Substring(0, goCloneUIPrefabs.name.Length-7);              
        //根据UI窗体名称，加载预设克隆体
        if (_traCanvasTransform != null && goCloneUIPrefabs != null)
        {
            baseUIForm = goCloneUIPrefabs.GetComponent<BaseUIForm>();
            if (baseUIForm == null)
            {
                Debug.Log("BaseUIForm==null !!,请先确认窗体预设对象上是否加载了BaseUIForm的子类脚本");
                return null;
            }
            switch (baseUIForm.CurrentUIType.uiFormType)
            {
                case UIFormType.Normal:              //普通窗体节点
                    goCloneUIPrefabs.transform.SetParent(_traNormal, false);
                    break;
                case UIFormType.Fixed:                //固定窗体节点
                    goCloneUIPrefabs.transform.SetParent(_traFixed, false);
                    break;
                case UIFormType.PopUp:                //弹出窗体节点
                    goCloneUIPrefabs.transform.SetParent(_traPopUp, false);
                    break;
                default:
                    break;
            }
            //设置隐藏
            goCloneUIPrefabs.SetActive(false);
            //加入缓存
            _DicAllUIForms.Add(uiFormName, baseUIForm);
            return baseUIForm;
        }
        else
        {
            Debug.Log("_traCanvasTransform==null Or goCloneUIPrefabs==null");
        }
        //设置UI克隆体的父节点
        Debug.Log("出现不可预估的错误，请检查，参数 UIFormaName= " + uiFormName);
        return null;
    }

    /// <summary>
    /// 把当前窗体加载到“当前窗体”集合中
    /// </summary>
    /// <param name="uiFormName">窗体预设的名称</param>
    private void LoadUIToCurrentChche(string uiFormName)
    {
        BaseUIForm baseUIForm;                            //UI窗体基类
        BaseUIForm baseUIFormFromAllCahce;             //从“所有窗体集合”中得到的窗体
                                                       //如果“正在显示”的集合中，存在整个UI窗体，则直接返回
        _DicCurrentShowUIForms.TryGetValue(uiFormName, out baseUIForm);
        if (baseUIForm != null) return;
        //把当前窗体，加载到“正在显示”集合中
        _DicAllUIForms.TryGetValue(uiFormName, out baseUIFormFromAllCahce);
        if (baseUIFormFromAllCahce != null)
        {
            _DicCurrentShowUIForms.Add(uiFormName, baseUIFormFromAllCahce);
            baseUIFormFromAllCahce.Display();             //显示当前的窗体
        }
    }

    /// <summary>
    /// ui窗体的入栈
    /// </summary> 
    /// <param name="uiFormName">窗体的名称</param>
    private void PushUIFormToStack(string uiFormName)
    {
        BaseUIForm baseUIForm;
        //判断“栈”集合中是否有其他的窗体，有则“冻结”处理
        if (_stackCurrentUIForms.Count > 0)
        {
            BaseUIForm topUIForm = _stackCurrentUIForms.Peek();
            topUIForm.Freeze();
        }
        //判断“UI所有窗体”集合是否有指定的UI窗体，有则处理
        _DicAllUIForms.TryGetValue(uiFormName, out baseUIForm);

        if (baseUIForm != null)
        {
            baseUIForm.Display();
            _stackCurrentUIForms.Push(baseUIForm);  //如果有，隐藏掉，并入栈
        }
        else
            Debug.Log("baseUIForm==null,Please Check，参数 uiFromName: " + uiFormName);
        //把指定的UI窗体，入栈操作
    }


    /// <summary>
    /// 退出指定UI窗体
    /// </summary>
    /// <param name="uiFormName"></param>
    private void ExitUIForms(string uiFormName)
    {
        BaseUIForm baseUIForm;
        //"正在显示的集合"中如果没有记录，则直接返回
        _DicCurrentShowUIForms.TryGetValue(uiFormName, out baseUIForm);
        if (baseUIForm == null) return;
        //指定窗体，标记为隐藏状态，从“正在显示的集合中移除”
        baseUIForm.Hiding();
        _DicCurrentShowUIForms.Remove(uiFormName);

    }
    /// <summary>
    /// 反向切换属性窗体出栈逻辑
    /// </summary>
    private void PopUIForms()
    {
        if (_stackCurrentUIForms.Count >= 2)
        {
            //出栈
            BaseUIForm topUiForms = _stackCurrentUIForms.Pop();
            topUiForms.Hiding();
            //出站后，下一个窗体做“重新显示”处理
            BaseUIForm nextUIForm = _stackCurrentUIForms.Peek();
            nextUIForm.ReDisplay();
        }
        else if (_stackCurrentUIForms.Count == 1)
        {
            //出栈
            BaseUIForm topUiForms = _stackCurrentUIForms.Pop();
            topUiForms.Hiding();
        }
    }

    /// <summary>
    /// (”隐藏其他“属性)打开窗体且隐藏其他窗体
    /// </summary>
    /// <param name="strUIName">打开指定窗体的名称</param>
    private void EnterUIFormsAndHideOther(string strUIName)
    {
        BaseUIForm baseUIForm;          //ui窗体基类
        BaseUIForm baseUIFormAll;      //从集合中得到的UI窗体基类
        //参数检查
        if (string.IsNullOrEmpty(strUIName)) return;
        _DicCurrentShowUIForms.TryGetValue(strUIName, out baseUIForm);
        if (baseUIForm != null) return;
        //把“正在显示集合”与“栈集合”中的所有窗体都隐藏
        foreach (BaseUIForm baseUI in _DicCurrentShowUIForms.Values)
        {
            baseUI.Hiding();
        }
        foreach (BaseUIForm stackUI in _stackCurrentUIForms)
        {
            stackUI.Hiding();
        }
        //把当前从窗体加入到“正在显示窗体”的集合中，且做显示处理
        _DicAllUIForms.TryGetValue(strUIName, out baseUIFormAll);
        if (baseUIFormAll != null)
        {
            _DicCurrentShowUIForms.Add(strUIName, baseUIFormAll);
            baseUIFormAll.Display();
        }
    }



    /// <summary>
    /// (”隐藏其他“属性)关闭窗体且打开其他窗体
    /// </summary>
    /// <param name="strUIName">打开指定窗体的名称</param>
    private void ExitUIFormsAndHideOther(string strUIName)
    {
        BaseUIForm baseUIForm;          //ui窗体基类
        //参数检查
        if (string.IsNullOrEmpty(strUIName)) return;
        _DicCurrentShowUIForms.TryGetValue(strUIName, out baseUIForm);
        if (baseUIForm == null) return;
        //当前窗体显示状态，且“正在显示”集合中，移除本窗体
        baseUIForm.Hiding();
        _DicCurrentShowUIForms.Remove(strUIName);
        //把“正在显示集合”与“栈集合”中所有窗体都定义重新显示状态
        foreach (BaseUIForm baseUI in _DicCurrentShowUIForms.Values)
        {
            baseUI.ReDisplay();
        }
        foreach (BaseUIForm stackUI in _stackCurrentUIForms)
        {
            stackUI.ReDisplay();
        }
    }

    /// <summary>
    /// 是否清空“栈集合”中的数据
    /// </summary>
    /// <returns></returns>
    private bool ClearStackArray()
    {
        if (_stackCurrentUIForms != null && _stackCurrentUIForms.Count > 0)
        { 
            _stackCurrentUIForms.Clear();
            return true;
        }
        return false;   
    }

    private void InitUIFormPathData()
    {
        IConfigManager configManager = new ConfigManagerByJson(SysDefine.SYS_PATH_UIFORM_CONFIG_INFO);
        if (configManager != null)
            _DicFormsPaths = configManager.AppSetting;
    }

    #endregion

}
