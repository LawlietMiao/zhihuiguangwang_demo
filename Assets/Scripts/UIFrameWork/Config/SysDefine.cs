//脚本名称: SysDefine
//功能描述：
//作者：
//日期：2021/07/05 15:01:27

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SysDefine : MonoBehaviour
{

    /*  路径常量 */
    public const string SYS_PATH_CANVAS = "Canvas";
    public const string SYS_PATH_UIFORM_CONFIG_INFO = "UIFormConfigInfo";
    /*  标签常量 */
    public const string SYS_TAG_CANVAS = "_TagCanvas";
    public const string SYS_TAG_UICAMERA = "_TagUICamera";
    /*  节点常量 */
    public const string SYS_NORMAL_NODE = "Normal";
    public const string SYS_FIXED_NODE = "Fixed";
    public const string SYS_POPUP_NODE = "PopUp";
    public const string SYS_SCRIPTMANAGER_NODE = "_ScriptsMgr";

    /*  遮罩管理器中，透明度常量 */
    public const float SYS_UIMASK_LUCENCY_COLOR_RGB = 255 / 255F;
    public const float SYS_UIMASK_LUCENCY_COLOR_A = 0 / 255F;

    public const float SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB = 255 / 255F;
    public const float SYS_UIMASK_TRANS_LUCENCY_COLOR_A = 0 / 255F;

    public const float SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_RGB = 255 / 255F;
    public const float SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_A = 0 / 255F;
    /*  全局性的方法 */
    //Todo...

    /*  委托的定义 */
    //Todo
}


/// <summary>
/// UI生成位置信息
/// </summary>
    public enum UIFormType
    { 
    //普通窗口
    Normal,
    //固定窗口
    Fixed,
    //弹出窗口
    PopUp
    }

/// <summary>
/// 窗体的显示类型
/// </summary>
public enum UIFormShowMode
{
    //普通
    Normal,
    //反向切换
    ReverseChange,
    //隐藏其他
    HideOther
}

/// <summary>
/// UI窗体的透明度类型
/// </summary>
public enum UIFormLucenyType
{ 
    //完全透明，不能穿透
    Luceny,
    //半透明不能穿透
    Translucence,
    //低透明度，不能穿透
    Impenetrable,
    //可以穿透
    Pentrate
}