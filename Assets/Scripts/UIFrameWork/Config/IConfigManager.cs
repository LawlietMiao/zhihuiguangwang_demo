//脚本名称: IConfigManager
//功能描述：通用配置管理器接口
//作者：
//日期：2021/07/07 11:54:25

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public interface IConfigManager  
{
    /// <summary>
    /// 只读属性，应用设置
    /// 得到键值对集合数据
    /// </summary>
   Dictionary<string, string> AppSetting { get; }

    /// <summary>
    /// 得到配置文件（AppSetting）的最大数量
    /// </summary>
    /// <returns></returns>
    int GetAppSettingMaxNumber();

}
[System.Serializable]
internal class KeyValueInfo
{
    public List<KeyValueNode> ConfigInfo = null;
}
[System.Serializable]
internal class KeyValueNode
{
    public string key = null;
    public string value = null;
}

