//脚本名称: ResourceMgr
//功能描述：加载
//作者：尚苗苗
//日期：2021/07/05 16:36:30

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceMgr : MonoSingleton<ResourceMgr>
{

    private Hashtable ht = new Hashtable();

    /// <summary>
    /// 调用资源
    /// </summary>
    /// <param name="path">路径</param>
    /// <param name="isCatch">是否加入缓存</param>
    /// <returns></returns>
    public GameObject LoadAsset(string path,bool isCatch=true)
    {
        GameObject obj = LoadResource<GameObject>(path,isCatch);
        GameObject objClone = Instantiate(obj);
        if (objClone == null)
            Debug.LogError(path+"资源加载失败");
        return objClone; 
    }

    private T LoadResource<T>(string path,bool isCatch) where T:Object
    {
        if (ht.ContainsKey(path))
            return ht[path] as T;

        T TRecource = Resources.Load<T>(path);
        if (TRecource == null)
            Debug.LogError(GetType() + "GetInstance/TResource提取资源失败");
        else
        {
            if (isCatch)
            {
                if (!ht.ContainsKey(path))
                    ht.Add(path, TRecource);
                else
                    ht[path] = TRecource;
            }
        }
        return TRecource;
    }
}

