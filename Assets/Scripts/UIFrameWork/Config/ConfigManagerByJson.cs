//脚本名称: ConfigManagerByJson
//功能描述：基于Json配置文件的配置管理器
//作者：
//日期：2021/07/07 13:51:54

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigManagerByJson : IConfigManager
{
   //保存键值对应用集合
    private static Dictionary<string, string> _Appsetting;

    /// <summary>
    /// 只读属性，得到应用设置（键值对集合）
    /// </summary>
    public Dictionary<string, string> AppSetting
    {
        get { return _Appsetting; }
    }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="path"></param>
    public ConfigManagerByJson(string path)
    {
        _Appsetting = new Dictionary<string, string>();
        //初始化解析Json数据，加载到集合中
        InitAndAnalysisJson(path);
    }



    public int GetAppSettingMaxNumber()
    {
        if (_Appsetting != null && _Appsetting.Count > 1)
        {
            return _Appsetting.Count;
        }
        else
            return 0;
    }

    /// <summary>
    /// 初始化json数据，加载到集合中
    /// </summary>
    /// <param name="jsonPath"></param>
    private void InitAndAnalysisJson(string jsonPath)
    {
        TextAsset configInfo = null;
        KeyValueInfo keyValueInfoObj = null;
        if (string.IsNullOrEmpty(jsonPath)) return;

        try
        {
            configInfo = Resources.Load<TextAsset>(jsonPath);
            keyValueInfoObj = JsonUtility.FromJson<KeyValueInfo>(configInfo.text);
        }
        catch (System.Exception e)
        {
            throw new System.Exception(e.ToString());
        }

        foreach (KeyValueNode nodeInfo in keyValueInfoObj.ConfigInfo)
        {
            _Appsetting.Add(nodeInfo.key, nodeInfo.value);
        }
    }

}
