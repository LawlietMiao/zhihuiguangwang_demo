//脚本名称: BaseUIForm
//功能描述：1、所有UIForm的父类
//                 2、定义自己的生命周期
//                 1）Display显示
//                 2）Hiding隐藏
//                 3）ReDisplay再显示
//                 4）Freeze冻结
//作者：
//日期：2021/07/05 15:22:58

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUIForm : MonoBehaviour
{
    [SerializeField]
    private UIType currentUIType = new UIType();

    /// <summary>
    /// 当前UI的显示类型
    /// </summary>
    public UIType CurrentUIType { get => currentUIType; set => currentUIType = value; }

    #region 窗体的四种状态
    /// <summary>
    /// 显示
    /// </summary>
    public virtual void Display()
    {
        gameObject.SetActive(true);
        //设置模态窗体调用
        if (CurrentUIType.uiFormType == UIFormType.PopUp)
            UIMaskMgr.GetInstance.SetMaskWindow(gameObject, CurrentUIType.uiFormLucenyType);
    }
    /// <summary>
    /// 隐藏
    /// </summary>
    public virtual void Hiding()
    {
        gameObject.SetActive(false);
        if (CurrentUIType.uiFormType == UIFormType.PopUp)
            UIMaskMgr.GetInstance.CancelMaskWindow(gameObject, CurrentUIType.uiFormLucenyType);
    }
    /// <summary>
    /// 再显示
    /// </summary>
    public virtual void ReDisplay()
    {
        gameObject.SetActive(true);
        //取消模态窗体调用
        if (CurrentUIType.uiFormType == UIFormType.PopUp)
            UIMaskMgr.GetInstance.SetMaskWindow(gameObject, CurrentUIType.uiFormLucenyType);
    }
    /// <summary>
    /// 冻结
    /// </summary>
    public virtual void Freeze()
    {
        gameObject.SetActive(true);
    }
    #endregion


    #region 封装子类常用的方法
    protected void RigisterButtonObjectEvent(string buttonName)
    {

    }
    /// <summary>
    /// 打开UI窗体
    /// </summary>
    /// <param name="uiFormName"></param>
    protected void OpenUIForm(string uiFormName)
    {
        UIManager.GetInstance.ShowUIForms(uiFormName);
    }
    /// <summary>
    /// 关闭当前UI窗体
    /// </summary>
    /// <param name="uiFormName"></param>
    protected void CloseUIForm()
    {
        string strUIFormName = string.Empty;             //处理后的UIForm名称
        strUIFormName = GetType().ToString();            //命名空间+类名
        int intPosition = -1;
        intPosition = strUIFormName.IndexOf(',');
        if (intPosition != -1)
            strUIFormName = strUIFormName.Substring(intPosition + 1);
        UIManager.GetInstance.CloseUIForms(strUIFormName);
    }
    #endregion
}
