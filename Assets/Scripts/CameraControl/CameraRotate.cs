using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public Transform target;
    public float distance = 7.0f;
    public float LerpSpeed = 20;
    private float eulerAngles_x;
    private float eulerAngles_y;
    public float distanceMax = 10;
    public float distanceMin = 3;
    public float xSpeed = 2;
    public float ySpeed = 2;
    public int yMaxLimit = 360;
    public int yMinLimit = -360;
    Vector3 Teme;
    float XX, YY;
    Vector3 screenPoint, offset, OldPoint;
    public float MouseScrollWheelSensitivity = 1.0f;
    public LayerMask CollisionLayerMask;
    void Start()
    {
        Vector3 eulerAngles = this.transform.eulerAngles;
        this.eulerAngles_x = eulerAngles.y;
        this.eulerAngles_y = eulerAngles.x;
    }
    void Update()
    { 
        //鼠标滚轮按下，调整视角
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            OldPoint = Input.mousePosition;
        }
        else if (Input.GetKey(KeyCode.Mouse2))
        {
            Vector3 Tagetpos = target.transform.localPosition;
            Vector3 MousPOS = Input.mousePosition;
            if (Input.mousePosition == OldPoint) return;
            Vector3 curPosition = (OldPoint - MousPOS) + Camera.main.WorldToScreenPoint(target.transform.localPosition);
            Vector3 pos = Camera.main.ScreenToWorldPoint(curPosition);
            target.transform.localPosition = new Vector3(Mathf.Clamp(pos.x, 0.4f, 2.3f), Mathf.Clamp(pos.y, 0.6f, 1.84f), Mathf.Clamp(pos.z, -2f, 0));
            OldPoint = Input.mousePosition;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse2))
        {
            Cursor.SetCursor(Resources.Load<Texture2D>("Mouse/shouzhi"), Vector2.zero, CursorMode.ForceSoftware);
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            XX = Input.mousePosition.x;
            YY = Input.mousePosition.y;
        }
        else if (Input.GetKey(KeyCode.Mouse1))
        {
            this.eulerAngles_x += (Input.mousePosition.x - XX) * Time.deltaTime * this.xSpeed;
            this.eulerAngles_y -= (Input.mousePosition.y - YY) * Time.deltaTime * this.ySpeed;
            this.eulerAngles_y = ClampAngle(this.eulerAngles_y, (float)this.yMinLimit, (float)this.yMaxLimit);
            XX = Input.mousePosition.x;
            YY = Input.mousePosition.y;
        }
        this.distance = Mathf.Clamp(this.distance - (Input.GetAxis("Mouse ScrollWheel") * MouseScrollWheelSensitivity), (float)this.distanceMin, (float)this.distanceMax);
        Quaternion quaternion = Quaternion.Euler(this.eulerAngles_y, this.eulerAngles_x, (float)0);
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Linecast(this.target.position, this.transform.position, out hitInfo, this.CollisionLayerMask))
        {
            this.distance = hitInfo.distance - 0.05f;
        }
        Vector3 vector = ((Vector3)(quaternion * new Vector3(0, 0, -this.distance))) + this.target.position;
        this.transform.position = Vector3.Lerp(this.transform.position, vector, Time.deltaTime* LerpSpeed);
        transform.LookAt(target);
    }
    public float ClampAngle(float angle, float min, float max)
    {
        while (angle < -360)
        {
            angle += 360;
        }
        while (angle > 360)
        {
            angle -= 360;
        }
        return Mathf.Clamp(angle, min, max);
    } 
}
