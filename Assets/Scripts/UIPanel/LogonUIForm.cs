//脚本名称: LogonUIForm
//功能描述：
//作者：
//日期：2021/07/06 09:06:08

using UnityEngine.UI;

public class LogonUIForm : BaseUIForm
{
    public Button sureBtn;


    private void Start()
    {
        sureBtn.onClick.AddListener(OnSureBtnClick);
    }

    /// <summary>
    /// 确认按钮点击
    /// </summary>
    private void OnSureBtnClick()
    {
        UIManager.GetInstance.ShowUIForms("PopUpUIForm");
    }
}
