//脚本名称: UnityHelper
//功能描述：Unity常用工具
//作者：
//日期：2021/07/06 17:33:18

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityHelper : MonoBehaviour
{
    /// <summary>
    /// 层级视图查找
    /// </summary>
    /// <param name="goParent">父对象</param>
    /// <param name="childName">查找的子对象</param>
    /// <returns></returns>
    public static Transform FindTheChildNode(Transform goParent,string childName)
    {
        Transform searchTrans= goParent.Find(childName);
        if (searchTrans == null)
        {
            foreach (Transform trans in goParent.transform)
            {
                searchTrans= FindTheChildNode(trans,childName);
                if (searchTrans != null)
                    return searchTrans;
            }
        }
        return searchTrans;
    }


    /// <summary>
    /// 获取子节点(对象)脚本
    /// </summary>
    /// <typeparam name="T">泛型</typeparam>
    /// <param name="goParent">父对象</param>
    /// <param name="childName">子对象名称</param>
    /// <returns></returns>
    public static T GetTheChildNodeComponentScripts<T>(Transform goParent, string childName) where T: Component
    {
        Transform searchTransformNode = null;          //查找特定子对象

        searchTransformNode = FindTheChildNode(goParent, childName);
        if (searchTransformNode != null)
            return searchTransformNode.GetComponent<T>();
        else
            return null;
    }
    /// <summary>
    /// 给子节点添加脚本
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="goParent"></param>
    /// <param name="childName"></param>
    /// <returns></returns>
    public static T AddChildNodeComponent<T>(Transform goParent, string childName) where T : Component
    {
        //查找特定子节点
        Transform searchTransformNode = null;
        searchTransformNode = FindTheChildNode(goParent, childName);
        //查找成功，如果有相同的脚本，先删除，再添加
        if (searchTransformNode != null)
        {
            T[] conponentScriptsArray = searchTransformNode.GetComponents<T>();
             for (int i = 0; i < conponentScriptsArray.Length; i++)
            {
                if (conponentScriptsArray[i] != null)
                    Destroy(conponentScriptsArray[i]);
            }
            return searchTransformNode.gameObject.AddComponent<T>();
        }

        //查找不成功，返回null
        else
            return null;
    }
 
    /// <summary>
    /// 给自己点添加父节点
    /// </summary>
    /// <param name="parents">父对象的方位</param>
    /// <param name="child">子对象的方位</param>
    public static void AddChildNodeToParentNode(Transform parents,Transform child)
    {
        child.SetParent(parents,false);
        child.localPosition = Vector3.zero;
        child.localScale = Vector3.one;
        child.localEulerAngles = Vector3.zero;
    }

}
