//脚本名称: UIMaskMgr
//功能描述：UI遮罩管理器，负责“弹出窗体”模态显示的实现
//作者：
//日期：2021/07/07 10:14:58

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMaskMgr : MonoSingleton<UIMaskMgr>
{
    /*  字段 */


    //UI根节点对象
    private GameObject _GoCanvasRoot = null;
    //UI脚本节点对象
    private Transform _TraUIScritpsNode = null;
    //顶层面板
    private GameObject _GoTopPanel = null;
    //遮罩面板
    private GameObject _GoMaskPanel = null;
    //UI摄像机
    private Camera _UICamera;
    //UI摄像机“层深”
    private float _OriginUICameraDepth;

    private void Awake()
    {
        _GoCanvasRoot = GameObject.FindGameObjectWithTag(SysDefine.SYS_TAG_CANVAS);
        _TraUIScritpsNode = UnityHelper.FindTheChildNode(_GoCanvasRoot.transform,SysDefine.SYS_SCRIPTMANAGER_NODE);

        //把本脚本实例，作为“脚本节点对象”的子节点
        UnityHelper.AddChildNodeToParentNode(_TraUIScritpsNode,this.transform);

        //得到”顶层面板“、“遮罩面板”
        _GoTopPanel = _GoCanvasRoot;
        _GoMaskPanel = UnityHelper.FindTheChildNode(_GoCanvasRoot.transform,"UIMaskPanel").gameObject;
        //得到UI摄像机原始的“层深”
        _UICamera = GameObject.FindGameObjectWithTag(SysDefine.SYS_TAG_UICAMERA).GetComponent<Camera>();
        if (_UICamera != null)
            _OriginUICameraDepth = _UICamera.depth;
        else
            Debug.Log(GetType()+ "Start()/UI_Camera is Null！ Please Cheack！！");


    }


    /// <summary>
    /// 设置遮罩状态
    /// </summary>
    /// <param name="goDisplayUIForms">需要显示的UI窗体</param>
    /// <param name="lucenyType">显示透明度属性</param>
    public void SetMaskWindow(GameObject goDisplayUIForms,UIFormLucenyType lucenyType=UIFormLucenyType.Luceny)
    {
        //顶层窗体下移
        _GoTopPanel.transform.SetAsLastSibling();
        //启用遮罩窗体以及设置透明度
        _GoMaskPanel.SetActive(true);
        Color color=new Color();
        switch (lucenyType)
        {
            //完全透明，不能穿透
            case UIFormLucenyType.Luceny:
                 color = new Color(SysDefine.SYS_UIMASK_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_LUCENCY_COLOR_A);
                break;
            //半透明，不能穿透
            case UIFormLucenyType.Translucence:
                color = new Color(SysDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_A);
                break;
            //低透明，不能穿透
            case UIFormLucenyType.Impenetrable:
                color = new Color(SysDefine.SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_RGB, SysDefine.SYS_UIMASK_IMPENETRABLE_LUCENCY_COLOR_A);
                break;
            //完全透明，能穿透
            case UIFormLucenyType.Pentrate:
                if (_GoMaskPanel.activeSelf)
                    _GoMaskPanel.SetActive(false);
                break;
            default:
                break;
        }
        _GoMaskPanel.GetComponent<Image>().color = color;
        //遮罩窗体下移
        _GoMaskPanel.transform.SetAsLastSibling();
        //显示窗体的下移
        goDisplayUIForms.transform.SetAsLastSibling();
        //增加当前UI摄像机的层深
        if (_UICamera != null)
            _UICamera.depth = _UICamera.depth + 100;
    }

    /// <summary>
    /// 取消遮罩状态
    /// </summary>
    /// <param name="goDisplayUIForms"></param>
    /// <param name="lucenyType"></param>
    public void CancelMaskWindow(GameObject goDisplayUIForms, UIFormLucenyType lucenyType = UIFormLucenyType.Luceny)
    {
        //顶层窗体上移
        _GoTopPanel.transform.SetAsFirstSibling();

        //遮罩窗体隐藏
        if (_GoMaskPanel.activeSelf)
            _GoMaskPanel.SetActive(false);


        //恢复当前UI摄像机的层深
        if (_UICamera != null)
            _UICamera.depth = _UICamera.depth - 100;
    }
}
