//脚本名称: MonoSingleton
//功能描述：单例泛型
//作者：
//日期：2021/07/05 15:03:40

using System.Collections;
using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T getInstance;

    public static T GetInstance {
        get {
            getInstance = FindObjectOfType<T>();
            if (getInstance == null)
            {
                getInstance = new GameObject(typeof(T).Name).AddComponent<T>();
            }
            return getInstance;
        }
    }
}
