﻿using UnityEngine;
using UnityEditor;
using System.IO;

[System.Obsolete]
public class ScriptsHelper : AssetModificationProcessor
{
    private const string AuthorName = "";
    private const string CreatDate = "yyyy/MM/dd HH:mm:ss" ;

    private static void OnWillCreateAsset(string path)
    {
        path = path.Replace(".meta","");
        if (path.EndsWith(".cs"))
        {
            string allText = File.ReadAllText(path);
            allText = allText.Replace("#AuthorName#", AuthorName);
            allText = allText.Replace("#CreatDate#", System.DateTime.Now.ToString(CreatDate));
            File.WriteAllText(path, allText);
            AssetDatabase.Refresh();
        }
    }
}
